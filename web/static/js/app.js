// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.
"use strict";

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "deps/phoenix_html/web/static/js/phoenix_html"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

// import socket from "./socket"

let bg = document.getElementById("bg");
const WIDTH = 1920;
const HEIGHT = 1080;
bg.style.width = WIDTH + "px";
bg.style.height = HEIGHT + "px";

window.addEventListener("resize", throttledResize, false);

resize();

var resizeTimeout;
function throttledResize() {
  if(!resizeTimeout) {
    resizeTimeout = setTimeout(function() {
      resizeTimeout = null;

      resize();
    }, 66);
  }
}

function resize() {
  bg.style.left = (window.innerWidth - WIDTH) / 2.0 + "px";
}
