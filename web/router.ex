defmodule Rcd.Router do
  use Rcd.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
  end

  scope "/", Rcd do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/about", PageController, :about
    get "/contact", PageController, :contact
  end

  # Other scopes may use custom stacks.
  # scope "/api", Rcd do
  #   pipe_through :api
  # end
end
